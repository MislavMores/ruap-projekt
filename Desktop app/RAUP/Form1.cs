﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;  

namespace RAUP
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private async void btnPredict_Click(object sender, EventArgs e)
        {
            try
            {
                double processorSpeed = Convert.ToDouble(txtCPU.Text);
                long ramSize = Convert.ToInt64(txtRAM.Text);
                string brand = txtBrand.Text;
                long storageCapacity = Convert.ToInt64(txtStorage.Text);
                double screenSize = Convert.ToDouble(txtScreenSize.Text);
                double weight = Convert.ToDouble(txtWeight.Text);

 
                var payload = new
                {
                    Inputs = new
                    {
                        data = new[]
                        {
                    new
                    {
                        Brand = brand,
                        Processor_Speed = processorSpeed,
                        RAM_Size = ramSize,
                        Storage_Capacity = storageCapacity,
                        Screen_Size = screenSize,
                        Weight = weight
                    }
                }
                    }
                };
                string jsonData = JsonConvert.SerializeObject(payload);

                var result = await InvokeRequestResponseService(jsonData);

                var result2 = JsonConvert.DeserializeObject<Dictionary<string, List<string>>>(result);
                string result3 = result2["Results"].FirstOrDefault();
                lblResult.Text = $"{result3}   Russian rubles";
                double result4 = Convert.ToDouble(result3) / 100;
                lblResult2.Text = $"{result4}   Eur";



            }
            catch (Exception ex)
            {
                MessageBox.Show("Greška pri slanju zahtjeva: " + ex.Message);
            }
        }

        static async Task<string> InvokeRequestResponseService(string jsonData)
        {
            var handler = new HttpClientHandler()
            {
                ClientCertificateOptions = ClientCertificateOption.Manual,
                ServerCertificateCustomValidationCallback = (httpRequestMessage, cert, cetChain, policyErrors) => { return true; }
            };

            using (var client = new HttpClient(handler))
            {
                client.BaseAddress = new Uri("http://5e43a41e-4e5a-4229-8aca-e3b8272db5eb.westeurope.azurecontainer.io/score");
                var content = new StringContent(jsonData, Encoding.UTF8, "application/json");

                HttpResponseMessage response = await client.PostAsync("", content);

                if (response.IsSuccessStatusCode)
                {
                    return await response.Content.ReadAsStringAsync();
                }
                else
                {
                    string errorResponse = await response.Content.ReadAsStringAsync();
                    throw new Exception($"Greška: {response.StatusCode}, Detalji: {errorResponse}");
                }
            }
        }
    }
}

