import pandas as pd
import numpy as np
import seaborn as sns
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import StandardScaler, LabelEncoder
import warnings 

# Remove warnings
warnings.filterwarnings("ignore")

# Load the data
df = pd.read_csv('/kaggle/input/laptoppriceprediction/Laptop_price.csv')

# Exploratory Data Analysis (Optional - not in final model code)
print(df.head())
print(df.tail())
print(df.info())
print(df.describe())
print(df.isnull().sum())
print(df.duplicated().sum())

# Visualizations (Optional - not in final model code)
sns.boxplot(data=df, x='Brand', y='Price')
sns.lineplot(data=df, x='Brand', y='Price')
sns.lineplot(data=df, x='Processor_Speed', y='Price')
sns.barplot(data=df, x='Processor_Speed', y='Brand')
sns.histplot(data=df)
sns.histplot(data=df, x='Processor_Speed', y='Price')
sns.lineplot(data=df,x='RAM_Size', y='Price')
sns.lineplot(data=df,x='Storage_Capacity', y='Price')
sns.lineplot(data=df, x='Weight', y='Price')
sns.boxplot(data=df,x='Storage_Capacity', y='Price')
sns.boxenplot(data=df,x='Storage_Capacity', y='Price')
sns.barplot(data=df,x='Storage_Capacity', y='Price')
sns.barplot(data=df,x='Brand', y='Price')
sns.barplot(data=df, x='Processor_Speed', y='Price')

# Convert string columns to numeric using Label Encoding
for column in df.columns:
    if df[column].dtype == 'object':
        le = LabelEncoder()
        df[column] = le.fit_transform(df[column])

# Define the feature columns and target variable
numeric_cols = ['Processor_Speed', 'RAM_Size', 'Storage_Capacity', 'Screen_Size', 'Weight', 'Price']
X = df[numeric_cols]
y = df['Price']

# Split the data into training and testing sets
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# Standardize the feature data
scaler = StandardScaler()
X_train_scaled = scaler.fit_transform(X_train)
X_test_scaled = scaler.transform(X_test)

# Train the linear regression model
model = LinearRegression()
model.fit(X_train_scaled, y_train)

# Evaluate the model on the test set
r_squared = model.score(X_test_scaled, y_test)
print(f'R-squared: {r_squared:.2f}')
